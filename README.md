# Shopping List

## Description
Web-application that allows you create and manage shopping list.

## Technologies
Python(3.7.0)
Django(2.2.4)
Django-Rest-Framework(3.10.2)

### Go to the cloned repository and install requirement project's packages
```
pip install -r requirements.txt

### Django
Go to the folder with `manage.py` file, run Shoping List.api 
```
python manage.py runserver
