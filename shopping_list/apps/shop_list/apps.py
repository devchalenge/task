from django.apps import AppConfig


class ShopListConfig(AppConfig):
    name = 'shopping_list.apps.shop_list'
